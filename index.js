// de55460c-daa5-4c81-9e5d-6dd70d3b0f2f
let express = require("express");
let app = express();

app.get('/', (req, res) => {
    res.json("Da do day")
})

app.get('/sum', (req, res) => {
    var numA = (Number)(req.query.numA);
    var numB = (Number)(req.query.numB);
    var result = numA + numB;
    res.json({ sum: result });
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, function(){
    console.log(`App listening on port ${PORT}`);
})


